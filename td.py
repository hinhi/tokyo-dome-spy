import argparse
import time
from itertools import product, chain

import requests
from bs4 import BeautifulSoup

URL = 'https://www.nikkansports.com/baseball/professional/score/'


def spy():
    html = requests.get(URL).text
    soup = BeautifulSoup(html, 'html.parser')
    for score_box in soup.find_all('div', class_="scoreBox"):
        if '東京ドーム' not in score_box.text:
            continue
        table = score_box.find('table', class_='scoreTable')
        td = table.find_all('td', class_=None)
        state = (0, '', '')
        n = len(td) // 2
        td = chain.from_iterable(zip(td[:n], td[n:]))
        for t, (a, b) in zip(td, product(range(1, 20), '表裏')):
            print(t, a, b)
            try:
                _ = int(t.text)
                state = (a, b, t.text)
            except ValueError:
                break
        return state, '試合終了' in score_box.text


def notice(slack_url, msg):
    requests.post(slack_url, json={'text': msg, 'channel': '#999_random'})


def run(args):
    fail = 0
    now = ()
    while True:
        new = spy()
        if new is None:
            fail += 1
            if fail >= 3:
                return
        elif now != new:
            now = new
            (a, b, c), fin = now
            if fin:
                msg = '東京ドームの試合が終わりました'
            elif c == '0':
                msg = f'東京ドームの試合で{a}回の{b}が終わりました'
            else:
                msg = f'東京ドームの試合で{a}回の{b}が動きました'
            notice(args.slack_url, msg)
            if fin:
                break
        else:
            _, fin = now
            if fin:
                break
        time.sleep(args.sleep)


def main():
    p = argparse.ArgumentParser()
    p.add_argument('slack_url')
    p.add_argument('--sleep',
                   type=int,
                   default=60)
    args = p.parse_args()
    run(args)


if __name__ == '__main__':
    main()
